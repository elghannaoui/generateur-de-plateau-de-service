/* > Insert license here < */


/***********************************/
/* VARIABLES YOU NEED TO FILL */
/**********************************/

/* Configurable parameters in millimeters */
lateral_planks_thickness = 10;
bottom_plank_thickness = 4.2; // Add 0.5mm to the size as a mounting marge.
bottom_PMMA_thickness = 4; // Optional transparent protection at bottom of the tray.
tray_length = 480; // Ratio 16:10 by default.
tray_width = 300;
tray_height = 50;
handle_length = 80;
handle_height = 20;

add_bottom_PMMA_plate = true; // Set to "false" to disable.
display = "3D"; // Set to "2D" to see a plane view.


/*************************/
/* AUTOMATED SCRIPT */
/***********************/

include <lasercut.scad>;

/* Automatic parameters */

inside_tray_length = tray_length - 2*lateral_planks_thickness;
inside_tray_width = tray_width - 2*lateral_planks_thickness;
bottom_plank_cutout_length = tray_width*24/100;
lengthways_plank_cutout_length = tray_length - bottom_plank_cutout_length*2;
widthways_plank_cutout_length = tray_width - bottom_plank_cutout_length*2;
cutout_margin = 5;
handle_y_position = bottom_plank_thickness+cutout_margin+((tray_height-bottom_plank_thickness-cutout_margin)/2-handle_height/2);

module bottom_plank() {
    lasercutoutSquare(
        thickness = bottom_plank_thickness,
        x = tray_length,
        y = tray_width,
        cutouts = [
            // top
            [0, tray_width-lateral_planks_thickness, bottom_plank_cutout_length, lateral_planks_thickness],
            [tray_length-bottom_plank_cutout_length, tray_width-lateral_planks_thickness, bottom_plank_cutout_length, lateral_planks_thickness],
            [tray_length/2-bottom_plank_cutout_length/2, tray_width-lateral_planks_thickness, bottom_plank_cutout_length, lateral_planks_thickness],
            // bottom
            [0, 0, bottom_plank_cutout_length, lateral_planks_thickness],
            [tray_length-bottom_plank_cutout_length, 0, bottom_plank_cutout_length, lateral_planks_thickness],
            [tray_length/2-bottom_plank_cutout_length/2, 0, bottom_plank_cutout_length, lateral_planks_thickness],
            // left
            [0, 0, lateral_planks_thickness, bottom_plank_cutout_length],
            [0, tray_width-bottom_plank_cutout_length, lateral_planks_thickness, bottom_plank_cutout_length],
            // right
            [tray_length-lateral_planks_thickness, 0, lateral_planks_thickness, bottom_plank_cutout_length],
            [tray_length-lateral_planks_thickness, tray_width-bottom_plank_cutout_length, lateral_planks_thickness, bottom_plank_cutout_length]
        ]
    );
}

module lengthways_plank() {
    // finger_joints are added to the plank length.
    translate([lateral_planks_thickness, 0, 0])
    lasercutoutSquare(
        thickness = lateral_planks_thickness,
        x = inside_tray_length,
        y = tray_height,
        cutouts = [
            // bottom
            [bottom_plank_cutout_length-lateral_planks_thickness, cutout_margin, lengthways_plank_cutout_length-lengthways_plank_cutout_length/2-bottom_plank_cutout_length/2, bottom_plank_thickness],
            [bottom_plank_cutout_length-lateral_planks_thickness+lengthways_plank_cutout_length/2+bottom_plank_cutout_length/2, cutout_margin, lengthways_plank_cutout_length-lengthways_plank_cutout_length/2-bottom_plank_cutout_length/2, bottom_plank_thickness]
        ],
        finger_joints = [
            [LEFT, 1, 4],
            [RIGHT, 1, 4]
        ]
    );
}

module widthways_plank() {
    translate([lateral_planks_thickness, 0, 0])
    lasercutoutSquare(
        thickness = lateral_planks_thickness,
        x = inside_tray_width,
        y = tray_height,
        cutouts = [
            // bottom
            [bottom_plank_cutout_length-lateral_planks_thickness, cutout_margin, widthways_plank_cutout_length, bottom_plank_thickness],
            // handle
            [inside_tray_width/2-handle_length/2, handle_y_position, handle_length, handle_height]
        ],
        finger_joints = [
            [LEFT, 1, 4],
            [RIGHT, 1, 4]
        ],
        circles_remove = [
            [handle_height/2, inside_tray_width/2-handle_length/2, handle_y_position+handle_height/2],
            [handle_height/2, inside_tray_width/2+handle_length/2, handle_y_position+handle_height/2]
        ]
    );
}

module bottom_PMMA_plate() {
    lasercutoutSquare(
        thickness = bottom_PMMA_thickness,
        x = inside_tray_length - 0.5,
        y = inside_tray_width - 0.5
    );
}

/* Draw parts */
if (display == "2D") {
    bottom_plank();
    translate([0, tray_width+10, 0]) lengthways_plank();
    rotate([0, 0, 180]) translate([-tray_length, 10, 0]) lengthways_plank();
    translate([-10, 0, 0]) rotate([0, 0, 90]) widthways_plank();
    rotate([0, 0, -90]) translate([-tray_width, tray_length+10, 0]) widthways_plank();
    if (add_bottom_PMMA_plate) {
        translate([tray_length+20+tray_height, lateral_planks_thickness, 0]) bottom_PMMA_plate();
    }
} else {
    bottom_plank();
    translate([0, tray_width, -cutout_margin])
        rotate([90, 0, 0])
            lengthways_plank();
    translate([tray_length, 0, -cutout_margin])
        rotate([90, 0, 180])
            lengthways_plank();
    translate([0, 0, -cutout_margin])
        rotate([90, 0, 90])
            widthways_plank();
    translate([tray_length, tray_width, -cutout_margin])
        rotate([90, 0, -90])
            widthways_plank();
    if (add_bottom_PMMA_plate) {
        translate([lateral_planks_thickness, lateral_planks_thickness, bottom_plank_thickness]) bottom_PMMA_plate();
    }
}